<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use App\User;

class StaffRegisterController extends Controller
{
    public function index()
    {
        $hospitals = Hospital::all();
        return view('staffRegister.index', compact([
            'hospitals'
        ]));
    }
    public function store(Request $request)
    {
        // dd($request);
        $user_id = User::create([
            'name'=> $request->name,
            'email'=> $request->email,
            'address'=>$request->address,
            // 'hospital_id'=> $request->hospital[0],
            'password'=>\Illuminate\Support\Facades\Hash::make( $request->password ),
            // 'role'=> 'staff'
        ]);

        User::updateAfterInsertion($user_id, $request->hospital[0]);


        return redirect(route('home'));
    }
}
