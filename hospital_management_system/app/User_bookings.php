<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class User_bookings extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id',
        'hospital_id'
    ];

    public static function insertBookingInfo(Hospital $hospital, $userid){
        // dd($hospital->id);
        $booking = User_bookings::create([
            'user_id'=> $userid,
            'hospital_id'=> $hospital->id
        ]);
        return $booking;
    }

    public static function all_booking_number(){
        $hospital_id = User::where('hospital_id', auth()->user()->hospital_id)->get()[0]->hospital_id;
        return User_bookings::where('hospital_id', $hospital_id)->get()->count();
    }
}
