<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(HospitalSeeder::class);
        $this->call(HospitalBedsSeeder::class);
        $this->call(UserBookingsSeeder::class);
        $this->call(UserSeeder::class);
    }
}
