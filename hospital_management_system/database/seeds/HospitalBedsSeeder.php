<?php

use Illuminate\Database\Seeder;

class HospitalBedsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Hospital_beds::create([
            'hospital_id'=> 1,
            'total_beds'=> 100,
            'beds_available'=> 99,
            'beds_booked'=> 1
        ]);

        \App\Hospital_beds::create([
            'hospital_id'=> 2,
            'total_beds'=> 70,
            'beds_available'=> 70,
            'beds_booked'=> 0
        ]);

        \App\Hospital_beds::create([
            'hospital_id'=> 3,
            'total_beds'=> 120,
            'beds_available'=> 119,
            'beds_booked'=> 1
        ]);

        \App\Hospital_beds::create([
            'hospital_id'=> 4,
            'total_beds'=> 80,
            'beds_available'=> 80,
            'beds_booked'=> 0
        ]);
    }
}
