
@extends('layouts.components.layout')
@section('title','Hospital management | Book bed for COVID-19')


@section('main-content')

<div class="container mt-4 mx-2">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
    <h1 class="h3 mb-4 text-gray-800"> <span class="color-blue">{{ $hospital->name }}</span></h1>
    <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-list-ul fa-sm text-white"></i>
    </a>                  
    </div>
  
      <h5 class="card-title ml-2">Fight against COVID-19</h5>

      <!-- Tabs -->
      <!-- <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
        </li>
      </ul>
      <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">ONe</div>
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">TWO</div>
        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">THREE</div>
      </div> -->

      <p>charges</p>
      <p>Doctor timings</p>
      <p>services</p>

      <form action="{{ route('hospital_beds.book', $hospital) }}" method="POST">
        @csrf
        @method('PUT')
        <button class="btn border-primary view-btn" type="submit">
          <span class="gray-color">Book a Bed &nbsp; <i class="fas fa-procedures"></i></span> 
        </button>
      </form>
      

</div>
@endsection

<!-- <button type="button" class="btn btn-primary">
  Notifications <span class="badge badge-light">4</span>
</button> -->
