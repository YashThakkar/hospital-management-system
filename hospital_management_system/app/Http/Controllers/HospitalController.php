<?php

namespace App\Http\Controllers;

use App\Hospital;
use App\Hospital_beds;
use App\Notifications\BedBookMessage;
use App\Notifications\NewBedBooked;
use App\User_bookings;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::id())->get()[0];
        if($user->role === 'staff'){
            
            $hospital = Hospital::where('id', $user->hospital_id)->get()[0];
            $hospital_beds = Hospital_beds::where('hospital_id', $hospital->id)->get()[0];
            $notifications = auth()->user()->unreadNotifications;
            // dd($notifications);
            return view('hospital.index', compact([
                'hospital',
                'hospital_beds',
                'notifications'
            ]));
        }
        abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function show(Hospital $hospital)
    {
        return view('hospital.show', compact([
            'hospital'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function edit(Hospital $hospital)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hospital $hospital)
    {
        //
    }

    public function destroy(Hospital $hospital)
    {
        //
    }

    public function bookABed(Request $request, Hospital $hospital){

        Hospital_beds::updateBedCount($hospital);
        $booking = User_bookings::insertBookingInfo($hospital, Auth::user()->id);

        $hospital_staff_user = User::where('hospital_id', $hospital->id)->get()[0];
        // dd($hospital_staff_user);

        $user = Auth::user();
        
        $user->notify(new NewBedBooked(Auth::user(),$hospital,$booking));

        $hospital_staff_user->notify(new BedBookMessage(Auth::user(),$booking));

        session()->flash('success', "Bed Booked Successfully, Please Check your email!");
        return redirect(route('booking.index'));
    }

    public function clearNotification(){
        //will clear all notifications
        Auth::user()->unreadNotifications->markAsRead();
        return redirect()->back();
    }

}
