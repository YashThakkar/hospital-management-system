
<div class="col-md-2" id="sidebar">
    <div class="logo-area">
        @if(auth()->user()->isStaff())
            <div class="btn main-purple-back color-white booking-btn py-2 px-3">View Bookings
            <span class="badge badge-pill" style=" background-color: #fff!important;color:#713BDB;">{{ App\User_bookings::all_booking_number() }}  </span>
            </div>
        @endif
    </div>
    <div class="div mt-5">
        <ul>

            @if(auth()->user()->isUser())
            <!-- dd(URL::current()) -->
                <li class="@yield('active_home')"><a href="{{ route('user.index') }}">
                    <span class="fas fa-home hover-purple"></span>&nbsp; <span class="hover-black">Home</span></a>
                </li>
                <li class="@yield('active_bookings')">
                    <a href="{{ route('booking.index') }}">
                        <span class="fas fa-bed hover-purple"></span>&nbsp; <span class="hover-black">Bookings</span>
                        <span class="badge badge-pill" style=" background-color: #713BDB!important;color:#fff;">{{ App\User::booking_number() }}  </span>
                    </a>
                </li>
            @endif

            @if(auth()->user()->isStaff())
                <li class="@yield('active_home')"><a href="{{ route('hospital.index') }}"><span class="fas fa-home hover-purple"></span>&nbsp; <span class="hover-black">Home</span></a></li>
                <li  class="@yield('active_bookings')">
                    <a href="{{ route('booking.index') }}">
                        <span class="fas fa-bed hover-purple"></span>&nbsp; <span class="hover-black">Bookings</span>
                        <span class="badge badge-pill" style=" background-color: #713BDB!important;color:#fff;">{{ App\User_bookings::all_booking_number() }}  </span>
                    </a>
                </li>
                <li class="@yield('active_discharge')"><a href="{{ route('discharge.index') }}"><span class="fas fa-clipboard-list hover-purple"></span>&nbsp; <span class="hover-black">Discharge List</span></a></li>
            @endif

        </ul>
    </div>
</div>

@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js"></script>
@endsection