<?php

use Illuminate\Database\Seeder;

class HospitalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $hospital = \App\Hospital::where('email','Fortis@gmail.com')->get()->first();
        // if(!$hospital){
        //     \App\Hospital::create([
        //         'name'=>'Fortis Hospital',
        //         'address'=>'L.B.S. Road, Near D-Mart, Mulund(West)',
        //         'contact_no'=>'98137459',
        //         'email'=>'Fortis@gmail.com'
        //     ]);
        // } else {
        //     $hospital->update(['role'=>'admin']);
        // }

        \App\Hospital::create([
            'name'=>'Fortis Hospital',
            'address'=>'L.B.S. Road, Near D-Mart, Mulund(West)',
            'contact_no'=>'98137459',
            'email'=>'fortis@gmail.com'
        ]);

        \App\Hospital::create([
            'name'=>'Ashirwad Hospital',
            'address'=>'RRT. Road, Near market, Mulund(West)',
            'contact_no'=>'90237572',
            'email'=>'ashirwad@gmail.com'
        ]);

        \App\Hospital::create([
            'name'=>'Jupiter Hospital',
            'address'=>'Godbundar Road, Near NH-12',
            'contact_no'=>'8938634',
            'email'=>'jupiter@gmail.com'
        ]);

        \App\Hospital::create([
            'name'=>'ICare Hospital',
            'address'=>'laminton Road, Near silver hts, CST',
            'contact_no'=>'790423782',
            'email'=>'icare@gmail.com'
        ]);
    }
}
