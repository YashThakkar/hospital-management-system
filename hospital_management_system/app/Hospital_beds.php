<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital_beds extends Model
{
    public static function updateBedCount(Hospital $hospital){

        $hospital_bed = Hospital_beds::where('hospital_id', $hospital->id)->get()[0];
        Hospital_beds::where('hospital_id', $hospital->id)->update([
            'beds_available'=> $hospital_bed->beds_available - 1,
            'beds_booked'=> $hospital_bed->beds_booked + 1
         ]);
    }
    public static function dischargeBedProcess($hospital_id){
        $hospital_bed = Hospital_beds::where('hospital_id', $hospital_id)->get()[0];
        Hospital_beds::where('hospital_id', $hospital_id)->update([
            'beds_available'=> $hospital_bed->beds_available + 1,
            'beds_booked'=> $hospital_bed->beds_booked - 1
         ]);
    }
}
