
@extends('layouts.components.layout')
@section('title','Hospital management | Book bed for COVID-19')

@section('active_home')
    active_page
@endsection

@section('main-content')

<div class="container-fluid mt-4 mx-2">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
    <h1 class="h3 mb-4 text-gray-800"> {{$hospital->name}}</h1>
    <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-list-ul fa-sm text-white"></i>
    </a>                  
    </div>
 
        <h5 class="card-title ml-2">Under service of COVID-19</h5>

        <div class="row d-flex justify-content-center">
            <div class="col-md-3">
                <div class="info_box bg-success px-4 py-3">
                    <p class="heading m-0">Total Beds</p>
                    <div class=" line dark-green my-2"></div>
                    <div class="info_box_content mt-1 d-flex justify-content-between">
                        <span class="info total-bed-count">{{ $hospital_beds->total_beds }}</span> 
                        <span class="symbol fas fa-procedures"></span>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-3">
                <div class="info_box bg-danger px-4 py-3">
                    <p class="heading m-0">Available Beds</p>
                    <div class=" line dark-red my-2"></div>
                    <div class="info_box_content mt-1 d-flex justify-content-between">
                        <span class="info hospital-bed-count">{{ $hospital_beds->beds_available }}</span> 
                        <span class="symbol fas fa-procedures"></span>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-3">
                <div class="info_box bg-warning px-4 py-3">
                    <p class="heading m-0">Beds Booked</p>
                    <div class=" line dark-yellow my-2"></div>
                    <div class="info_box_content mt-1 d-flex justify-content-between">
                        <span class="info total-bed-booked-count">{{ $hospital_beds->beds_booked }}</span> 
                        <span class="symbol fas fa-procedures"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="info_box bg-info px-4 py-3">
                    <p class="heading m-0">COVID service</p>
                    <div class=" line dark-blue my-2"></div>
                    <div class="info_box_content mt-1 d-flex justify-content-between">
                        <span class="info-small ">Under the Covid-19 service</span> 
                        <span class="symbol fas fa-info-circle"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <h6 class="card-title text-muted mt-5">
                    Notifications   
                    <span class="badge badge-pill" style=" background-color: #713BDB!important;color:#fff; font-size: 12px!important;">{{ $notifications->count() }}  </span>
                </h6>
            </div>
            <div class="col-md-5 mt-5">
                @if($notifications->count() > 0)
                    <a href=" {{ route('notification.clear') }}" style="font-size: 13px;">clear all &nbsp;<i class="fas fa-trash"></i></a>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                @if($notifications->count() > 0)
                    @foreach($notifications as $notification)
                        <div class="row p-3 notification_row ml-1 mr-4 shadow my-2">
                            <div class="col-md-9 text-white   ">
                                <p class="m-0 m-1">Bed Booked by: {{ $notification->data["user"] }}</p>
                            </div>
                            <div class="col-md-3 text-white   ">
                                <p class="m-0 m-1">Booking Id: {{ $notification->data["bed"] }}</p>
                            </div>
                        </div>
                    @endforeach
                @else
                    <h5 class="text-muted mt-5 text-center pt-5">No New Notifications Yet &nbsp; <i class="fas fa-exclamation-circle"></i></h5>
                @endif
            </div>
            <div class="col-md-4">
                <div class="card shadow p-2 m-2 border-0 hospital-home-chart">
                    <canvas id="myChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        
 
</div>
@endsection
@section('page-level-scripts')
<script>
var tbc = $(".total-bed-count").html();
//console.log($(".total-bed-count").html());
var hbc = $(".hospital-bed-count").html();
var tbbc = $(".total-bed-booked-count").html();

var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Total Beds', 'Avail Beds', 'Beds Booked'],
        datasets: [{
            label: '# of Votes',
            // data: [tbc, hbc, tbbc],
            data: [tbc, hbc, tbbc],
            backgroundColor: [
                '#EB746F',
                '#99FF99',
                '#FFFF85'
            ],
            borderColor: [
                // 'red',
                // 'green',
                // 'yellow'
            ],
            borderWidth: 4
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                },
                gridLines: {
                  display: false
               }
            }],
            xAxes: [{
                ticks: {
                    beginAtZero: true
                },
                gridLines: {
                  display: false
               }
            }]
        }
    }
});
</script>

@endsection

