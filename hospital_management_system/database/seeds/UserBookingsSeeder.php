<?php

use Illuminate\Database\Seeder;

class UserBookingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User_bookings::create([
            'user_id'=> 2,
            'hospital_id'=> 3
        ]);

        \App\User_bookings::create([
            'user_id'=> 2,
            'hospital_id'=> 1
        ]);
    }
}
