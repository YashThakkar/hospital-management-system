<?php

namespace App\Http\Controllers;

use App\Hospital_beds;
use App\User_bookings;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBookingsController extends Controller
{
    public function destroy(User_bookings $booking, Request $request)
    {
        $booking->delete();
        Hospital_beds::dischargeBedProcess($booking->hospital_id);

        session()->flash('success', "Patient Discharged Successfully");
        return redirect()->back();
    }

    public function destroyPermanent($user_id, Request $request){
        $user = User_bookings::where('id', $user_id)->onlyTrashed()->get()[0];
        
        $user->forceDelete();

        session()->flash('success', "Discharged Booking Deleted Successfully");
        return redirect()->back();
    }
}
