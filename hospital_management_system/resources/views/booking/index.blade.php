
@extends('layouts.components.layout')
@section('title','Hospital management | Book bed for COVID-19')

@section('active_bookings')
    active_page
@endsection

@section('main-content')

<div class="container mt-4 mx-2">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
    <h1 class="h3 mb-4 text-gray-800">Your Bookings</h1>
    <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-list-ul fa-sm text-white"></i>
    </a>                  
    </div>
        <!-- <h5 class="card-title ml-2">
            All under COVID-19 service &nbsp;<i class="fas fa-shield-virus text-success" style="font-size: 18px;"></i>
        </h5> -->

        @if(sizeof($bookings) == 0)
            <h2 class="text-muted mt-5 text-center pt-5">Oops, No Bookings Yet &nbsp; <i class="fas fa-exclamation-circle"></i></h2>
        @endif

        @foreach($bookings as $booking)
            <form action=" {{ route('user_booking.destroy', $booking) }}" method="POST">
                @csrf
                @method('PUT')
            <?php

                $color = rand(1,3);

                $hospital = App\Hospital::where('id', $booking->hospital_id)->get()[0];
                $user = App\User::where('id', $booking->user_id)->get()[0];
            ?>
            
            <div class="row ml-2">
                <div class="col-md-8 leader-cards my-3 shadow p-0"  style="border-radius: 10px!important;">
                    <div class="name-box border_color_{{ $color }} px-2"  style="border-radius: 10px!important;">
                        <div class="row p-3 pb-0">
                            <div class="col-md-6 ">
                                <p class="m-0 member-name"><i class="fas fa-hospital"></i>&nbsp; {{ $hospital->name }}</p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p class="m-0 member-name m-0">Booking ID :&nbsp; {{ $booking->id }}</p>
                            </div>
                        </div>
                        
                        <div class="row p-2">
                            <div class="col-md-6">
                                <p class="hospital-info-color mb-2"><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp; {{ $hospital->address }}</p>
                                <p class="hospital-info-color mb-2"><i class="far fa-id-card"></i>&nbsp;&nbsp; {{ $hospital->contact_no }}</p>
                                <p class="hospital-info-color"><i class="fas fa-envelope"></i>&nbsp;&nbsp; {{ $hospital->email }}</p>
                            </div>
                            <div class="col-md-6 text-right pr-5">
                                <p class="text-success m-0" style="font-size: 50px; height: 65px;"><i class="fas fa-check-circle"></i></p>
                                <p class="text-muted m-0">Booked</p>
                            </div>
                        </div>
                        <div class="row pl-2 px-1 bg-gray" style="margin-left:-8px!important;margin-right:-8px!important;background: #E0E0E0!important;border-radius: 8px!important;" >
                            <div class="col-md-10 py-2">
                                <p class="text-muted m-0 m-1"><i class="fas fa-user"></i>&nbsp; {{$user->name}}</p>
                                <p class="text-muted m-0 m-1"><i class="fas fa-envelope"></i>&nbsp; {{$user->email}}</p>
                            </div>
                            <div class="col-md-2 p-3">
                                @if(auth()->user()->role === "staff")
                                    <button type="submit" class=" btn bg-danger text-white">Discharge</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </form>
            
        @endforeach
</div>
@endsection

