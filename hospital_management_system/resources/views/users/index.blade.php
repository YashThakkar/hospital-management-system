
@extends('layouts.components.layout')
@section('title','Hospital management | Book bed for COVID-19')

@section('active_home')
    active_page
@endsection

@section('main-content')

<div class="container mt-4 mx-2">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
    <h1 class="h3 mb-4 text-gray-800">Hospitals for COVID-19 <span class="color-blue"></span></h1>
    <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-list-ul fa-sm text-white"></i>
    </a>                  
    </div>
    <!-- <div class="card text-white bg-dark mb-3" style="max-width: 100%;"> -->
    <!-- <div class="card-body"> -->
        <h5 class="card-title ml-2">
            All under COVID-19 service &nbsp;<i class="fas fa-shield-virus text-success" style="font-size: 18px;"></i>
        </h5>

        @foreach($hospitals as $hospital)
               
            <?php
                // $user_info = App\User::where([
                //     'id' => $user->users_id,
                //     'role' => 'member'
                // ])->get();
            // dd($user_info[0]->name);
            $color = rand(1,3);
            ?>
            
            <div class="row ml-2">
                <div class="col-md-8 leader-cards my-3 shadow p-0"  style="border-radius: 10px!important;">
                    <div class="name-box border_color_{{ $color }} px-2"  style="border-radius: 10px!important;">
                        <div class="row p-3">
                            <div class="col-md-6 ">
                                <p class="m-0 member-name"><i class="fas fa-hospital"></i>&nbsp; {{ $hospital->name }}</p>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href=" {{ route('hospital.show', $hospital->id) }} " class="btn border-primary view-btn"><span class="gray-color">View</span> 
                                    <span class="fas fa-long-arrow-alt-right gray-color" style="line-height: 2;"></span>
                                </a>
                            </div>
                        </div>
                        <div class="row p-2">
                            <div class="col-md-6">
                                <p class="hospital-info-color mb-2"><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp; {{ $hospital->address }}</p>
                                <p class="hospital-info-color mb-2"><i class="far fa-id-card"></i>&nbsp;&nbsp; {{ $hospital->contact_no }}</p>
                                <p class="hospital-info-color"><i class="fas fa-envelope"></i>&nbsp;&nbsp; {{ $hospital->email }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        @endforeach
    <!-- </div> -->
    <!-- </div> -->
</div>
@endsection

<!-- <button type="button" class="btn btn-primary">
  Notifications <span class="badge badge-light">4</span>
</button> -->
