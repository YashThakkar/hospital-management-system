<?php

namespace App\Http\Controllers;

use App\User_bookings;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    public function index(){

        $user_role = User::where('id', Auth::id())->get()[0]->role;
        if($user_role === 'user'){
            $user_id = Auth::id();
            $bookings = User_bookings::where('user_id', $user_id)->get();
        }
        elseif($user_role === 'staff'){
            $bookings = User_bookings::where('hospital_id', Auth::user()->hospital_id)->get();
            // dd($bookings);
        }
        
        return view('booking.index', compact([
            'bookings'
        ]));
    }
}
