<?php

namespace App\Http\Controllers;

use App\User;
use App\User_bookings;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class DischargeController extends Controller
{
    public function index(){
        $logged_user = User::where('id', Auth::id())->get()[0];
        $users = User_bookings::where('hospital_id', $logged_user->hospital_id)->onlyTrashed()->get();
        return view('discharge.index', compact([
            'users'
        ]));
    }
}
