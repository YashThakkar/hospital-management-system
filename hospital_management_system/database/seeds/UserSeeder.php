<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'=> 'Jhon Doe',
            'email'=> 'jhon@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('jhondoe'),
            'role'=> 'staff',
            'hospital_id'=>3
        ]);

        \App\User::create([
            'name'=> 'Yash Thakkar',
            'email'=> 'yashthakkar2700@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar')
        ]);

        \App\User::create([
            'name'=> 'Nick',
            'email'=> 'nick@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
            'role'=> 'staff',
            'hospital_id'=>2
        ]);

        \App\User::create([
            'name'=> 'Jimmy Doe',
            'email'=> 'jimmy@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
            'role'=> 'staff',
            'hospital_id'=>1
        ]);

        \App\User::create([
            'name'=> 'Jane Doe',
            'email'=> 'jane@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
            'role'=> 'staff',
            'hospital_id'=>4
        ]);
    }
}
