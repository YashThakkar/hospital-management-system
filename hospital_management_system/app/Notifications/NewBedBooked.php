<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\User;
use App\Hospital;
use App\User_bookings;

class NewBedBooked extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Hospital $hospital, User_bookings $booking)
    {
        $this->user = $user;
        $this->hospital = $hospital;
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->hospital->name)
                    ->line('Bed Booked Successfully.')
                    ->line('Bed Booking Id: ' . $this->booking->id)
                    ->line('Booked on the name : ' . $this->user->name)
                    ->action('Notification Action', url('/'))
                    ->from($this->hospital->email, 'Sender')
                    ->line('Thank you for using our application!');
                    
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user'=> $this->user->name,
            'bed'=> $this->booking->id
        ];
    }
}
