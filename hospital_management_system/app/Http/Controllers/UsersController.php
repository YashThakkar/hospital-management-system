<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use App\User;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function index()
    {
        $user_role = User::where('id', Auth::id())->get()[0]->role;
        if($user_role != 'staff'){
            $hospitals = Hospital::all();
            return view('users.index', compact([
                'hospitals'
            ]));
        }
        abort(403);
    }
}
