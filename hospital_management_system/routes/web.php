<?php

use App\Http\Controllers\HospitalController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/staff_register', 'StaffRegisterController@index')->name('staff_register'); 
Route::post('/staff_register/store', 'StaffRegisterController@store')->name('staff_register.store');

Route::middleware(['auth'])->group(function(){ //, 'verifyuser'

    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('user', 'UsersController');
    Route::resource('hospital', 'HospitalController');
    Route::put('/hospital/{hospital}', 'HospitalController@show')->name('hospital.show');
    Route::put('/hospital/{hospital}/book', 'HospitalController@bookABed')->name('hospital_beds.book');
    Route::get('/booking', 'BookingController@index')->name('booking.index');

    Route::put('/hospital/booking/{booking}', 'UserBookingsController@destroy')->name('user_booking.destroy');

    Route::get('/discharge', 'DischargeController@index')->name('discharge.index');
    Route::put('/discharge/remove/{user_id}', 'UserBookingsController@destroyPermanent')->name('user_booking.remove');
    
    Route::get('/clear', 'HospitalController@clearNotification')->name('notification.clear');
});

